<?php

/*
	Author: Anthony Sychev [hello at dm211 dot com]
	
	git: https://bitbucket.org/klich3/unifi-socialwifi/src/master/
	tested on: 5.9.xxx
	
	script dependences:
		-curl
		-sqlite
		-php 5 
		-php apc module
*/


define('__BASE__', dirname((__FILE__))); 

//detect source and path correction
$port_live = ($_SERVER["SERVER_PORT"]) ? ':'.$_SERVER["SERVER_PORT"] : '';
$path_live = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
$path_live .= $_SERVER["SERVER_NAME"].dirname($_SERVER["PHP_SELF"]);
$path_live = preg_replace('/^(https?:\/\/)?(.+)$/', "$1$2", $path_live);

$port_local = ($_SERVER["SERVER_PORT"]) ? ':'.$_SERVER["SERVER_PORT"] : '';
$port_local = ($port_local !== 80) ? $port_local : '';
$path_local = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
$path_local .= $_SERVER["SERVER_NAME"].$port_local.dirname($_SERVER["PHP_SELF"]).'/';

$isLocal = (preg_match('/(too|8888)/', $_SERVER["HTTP_HOST"])) ? true:false;
$path = (!$isLocal) ? $path_live : $path_local;

define('__BASESITE__', $path); 

@session_start();

// Date settings
date_default_timezone_set('Europe/Madrid');

// ----- config
$CONFIG = array(
	'debug' => false,
	
	'unifyID' => 'canalsimunne.com', //web page of unify client
	
	'unify' => array(
		'unifiServer' => "https://192.168.1.77:8443",	//unify url server for authorization https://unifi.example.com:8443
		'unifiUser'   => "admin",
		'unifiPass'   => "13922061",
		'unifiSiteName' => "xkmb1dx3",//"xkmb1dx3",	 // guest/u/<sitename>/ by default is "default"
	),
	
	//http://192.168.1.88/guest/s/xkmb1dx3/?id=7c:d1:c3:e6:e9:75&ap=f0:9f:c2:33:3c:81&t=1549705312&url=http://google.com%2f&ssid=wifi_restaurant
	
	'tmpCookieDir' => 'tmp/unify_cookie_lua',
	
	'checkMailBySMTP' => false,
	
	'supportMail' => 'support@example.com', //
	'defaultPageRedirect' => 'http://canalsimunne.com', //default page on redirect
	'siteName' => 'Canals & Munné', //title on html
	
	'db' => array(
		'type' => 'sqlite',
		'path' =>  __BASE__."/db/unifi.sqlite",
	),
	
	'path' => array(
		'base' => __BASE__,
		'cgibin' => '/srv/www/cgi-bin/srep inmail', //cgi-bin inject emails DB
	),
	
	'times' => array(
		'byemail' => 240, //set time as acces by mail in minutes
	),
	
	'social' => array(
		//facebook
		'fb' => array(
			
		),
		
		//instagram
		'in' => array(
			
		),
	)
);
// ----- config

?>
