<?php

/*
	Author: Anthony Sychev [hello at dm211 dot com]
	
	git: https://bitbucket.org/klich3/unifi-socialwifi/src/master/
	tested on: 5.9.xxx
	
	script dependences:
		-curl
		-sqlite
		-php 5 
		-php apc module
*/

include_once("easypdo.sqlite.php");
include_once("smtp_validateEmail.class.php");

class unify {
	
	protected $cl_cfg;
	protected $cl_db;
	protected $cl_user_mac_id;
	protected $cl_response_url;
	
	//apc
	private $fn_ip_loging;
	private $fn_user_loging;
	private $fn_bantime_loging;
	private $fn_banattemp_loging;
	private $fn_ap_mac;
	private $fn_unify_cookie;
	
	//dates
	private $fn_now;
	private $fn_now_date;
	private $fn_today;
	
	/**
	 * __construct function.
	 *
	 * @access public
	 * @param mixed $fn_cfg
	 * @return void
	 */
	public function __construct($fn_cfg)
	{
		//cfg
		$this->cl_cfg = $fn_cfg;
		
		//params is parsed by htaccess, is nedeed for get sitename
		if(isset($_REQUEST['params']))
		{
			$fn_param_loc = explode('/', $_REQUEST['params']);
			
			//rewrite config option
			$this->cl_cfg['site']['unifiSiteName'] = end($fn_param_loc);
		}
		
		//check all functions
		if($fn_cfg['debug']) self::checkCompatibility();
		
		//by default init session
		if(!isset($_SESSION)) @session_start();
		
		//times
		$this->fn_now = time();
		$this->fn_now_date = date('Y-m-d H:i:s');
		$this->fn_today = date('Y-m-d');
		
		if(!isset($_SESSION['id'])) exit("Acceso directo no permitido.");
		
		$fn_redirect_url = (isset($_REQUEST['url'])) ? $_REQUEST['url'] : $fn_cfg['defaultPageRedirect'];
		
		$this->cl_user_mac_id = (isset($_SESSION['id'])) ? $_SESSION['id'] : $_REQUEST['id']; //mac adress
		$this->cl_response_url = (isset($_SESSION['url'])) ? $_SESSION['url'] : $fn_redirect_url; //response if login correct
		
		$this->fn_ap_mac = (isset($_SESSION['ap'])) ? $_SESSION['ap'] : $_REQUEST['ap']; //mac adress
		$this->fn_unify_cookie = (isset($_SESSION['unificookie'])) ? $_SESSION['unificookie'] : false;
		
		try {
			$this->cl_db = EasyPDO_SQLite::Instance($this->cl_cfg['db']['path']);
			
			$fn_d = $this->cl_db->FetchAll("
				SELECT *
				FROM `guests`
			");			
		} catch(PDOException $e) {
			throw new Exception('[CN: 400]');
		}
	}
	
	/**
	 * checkCompatibility function.
	 * comprobamos compatibilidad y las extensiones
	 *
	 * @access private
	 * @return void
	 */
	private function checkCompatibility()
	{
		//sql lite
		if(!class_exists('EasyPDO')) throw new Exception('[DB: 400]');
		if(!class_exists('EasyPDO_SQLite')) throw new Exception('[DB: 400]');
		//if(!class_exists('SMTP_validateEmail')) throw new Exception('[SMTP: 400]');
		
		//check if apache get so APC cache
		$fn_php_extensions = get_loaded_extensions();
		if(!in_array('apc', $fn_php_extensions)) throw new Exception('[APCu: 400]');
		
		//exist db
		if(!file_exists($this->cl_cfg['db']['path'])) throw new Exception('[DB: 400]');
		
		//write to log file
		$fn_out_put = "";
		
		if($_POST) $fn_out_put .= "[POST]-> ".print_r($_POST, true)."\n";
		if($_GET) $fn_out_put .= "[GET]-> ".print_r($_GET, true)."\n";
		if($_SERVER) $fn_out_put .= "[SERVER]-> ".print_r($_SERVER, true)."\n";
		$fn_out_put .= "[php://input]-> ".file_get_contents("php://input")."\n";
		
		file_put_contents("log/_log-call-php.txt", $fn_out_put."\n --------------------------------------------------------------- \n\n", FILE_APPEND);
		//write to log file
		
		//self::callCurl("listsites");
	}
	
	/**
	 * authByEmail function.
	 * Damos el acceso con email
	 *
	 * @access public
	 * @param mixed $fn_email
	 * @return void
	 */
	public function authByEmail($fn_email)
	{
		$fn_check_ban = self::checkBan($fn_email);
		
		if($fn_check_ban)
		{
			//baneado demasiados intentos
			exit(json_encode(array(
				'status' => 400,
				'message' => 'Demasiados intentos seguidos. No podra acceder en un tiempo incremental.',
			)));
		}else{
			//check filter email
			if(!filter_var($fn_email, FILTER_VALIDATE_EMAIL)) exit(json_encode(array(
				'status' => 400,
				'message' => 'El correo electrónico no es válido.',
			)));
			
			//borramos dias anteriores
			self::removeOthersDaysDB();
			
			//check si lo ha puesto hoy
			$fn_q = $this->cl_db->FetchArray("
				SELECT COUNT(*) as 'count'
				FROM `mails`
				WHERE `email`=:e
				AND `time`=:t
			", array(
				'e' => $fn_email,
				't' => $this->fn_today,
			));
			
			if(isset($fn_q['count']) && $fn_q['count'] !== "0") exit(json_encode(array(
				'status' => 400,
				'message' => 'Hoy ya se uso este correo una vez.',
			)));
			
			$fn_sender = $this->cl_cfg['supportMail'];
			$fn_results = false;
			
			//check smtp
			if($this->cl_cfg['checkMailBySMTP'])
			{
				$SMTP_Validator = new SMTP_validateEmail();
				$SMTP_Validator->debug = false;
				$fn_results = $SMTP_Validator->validate(array($fn_email), $fn_sender);
			}
			
			if($this->cl_cfg['checkMailBySMTP'] && !$fn_results[$fn_email]) exit(json_encode(array(
				'status' => 400,
				'message' => 'El correo electrónico no exíste.',
			)));
			
			/*
			//unban
			self::unBan();
			*/
			/*
			//add email to sql
			$fn_q = $this->cl_db->Fetch("
				INSERT INTO `mails` (`email`, `time`) 
				VALUES (:e, :t)
			", array(
				'e' => $fn_email,
				't' => $this->fn_today,
			));
			*/
			
			//send cgi
			/*
			self::callCGI('cgi', array(
				'email' => $fn_email,
			));
			*/
			file_put_contents(__BASE__."/db/from_wifi.txt", "{$this->fn_today}\t{$fn_email}\n", FILE_APPEND);

			//send request unify
			self::callCurl('unify', array(
				'time' => $this->cl_cfg['times']['byemail']
			));
			
			exit(json_encode(array(
				'status' => 200,
				'message' => "Todo perfecto, acceso de ({$this->cl_cfg['times']['byemail']} min.), redireccionado.",
				'data' => array(
					'url' => $this->cl_response_url,
				),
			)));
		}
	}
	
	/**
	 * callCGI function.
	 * inject to cgi
	 *
	 * @access private
	 * @param mixed $fn_args
	 * @return void
	 */
	/*
	private function callCGI($fn_args)
	{
		@exec("{$this->cl_cfg['path']['cgibin']} {$fn_args['email']}");
	}
	*/
	
	/**
	 * authByUsPass function.
	 * autentificacion con contraseña
	 *
	 * @access public
	 * @param mixed $fn_user
	 * @param mixed $fn_pass
	 * @return void
	 */
	public function authByUsPass($fn_user, $fn_pass)
	{
		//$fn_check_ban = self::checkBan($fn_user);
		
		$fn_check_ban = false;
		
		if($fn_check_ban)
		{
			//baneado demasiados intentos
			exit(json_encode(array(
				'status' => 400,
				'message' => 'Demasiados intentos seguidos. No podra acceder en un tiempo incremental.',
			)));
		}else{
			//check si lo ha puesto hoy
			$fn_q = $this->cl_db->FetchArray("
				SELECT COUNT(*) as 'count', `time`
				FROM `guests`
				WHERE `username`=:u
				AND `password`=:p
				LIMIT 1;
			", array(
				'u' => $fn_user,
				'p' => $fn_pass,
			));
			
			if($fn_q && isset($fn_q['count']) && $fn_q['count'] !== "0")
			{
				//send request unify
				self::callCurl('unify', array(
					'time' => $fn_q['time'],
				));
				
				exit(json_encode(array(
					'status' => 200,
					'message' => "Todo perfecto, acceso de ({$fn_q['time']} min.), redireccionado.",
					'data' => array(
						'url' => $this->cl_response_url,
					),
				)));
			}else{
				exit(json_encode(array(
					'status' => 400,
					'message' => 'Nombre y contraseña no son válidos.',
				)));
			}
		}
	}
	
	/**
	 * callCurl function.
	 * 
	 * @access protected
	 * @param mixed $fn_type
	 * @param bool $fn_args (default: false)
	 * @return void
	 */
	protected function callCurl($fn_type, $fn_args = false)
	{
		$fn_url;
		$fn_data_post = null;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		// Allow Self Signed Certs
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSLVERSION, 1);
		
		switch($fn_type)
		{
			case "unify":
				//$fn_args['time']
				
				// Set up cookies
				curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cl_cfg['tmpCookieDir']);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cl_cfg['tmpCookieDir']);
				
				$fn_url = "{$this->cl_cfg['unify']['unifiServer']}/api/login";
				
				$fn_data_post = json_encode(array(
					"password" => $this->cl_cfg['unify']['unifiPass'],
					"username" => $this->cl_cfg['unify']['unifiUser'],
				));
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			break;
			
			case "unifycmd":
				//$fn_args['data']
			
				// Send user to authorize and the time allowed
				$fn_url = "{$this->cl_cfg['unify']['unifiServer']}/api/s/{$this->cl_cfg['unify']['unifiSiteName']}/cmd/stamgr";

				$fn_data = json_encode($fn_args['data']);
				$fn_data_post = "json={$fn_data}";

				// Make the API Call
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			break;
			
			case "unifylogout":
				// Logout of the connection
				$fn_url = "{$this->cl_cfg['unify']['unifiServer']}/logout";
				unset($this->fn_unify_cookie);
			break;
			
			case "block":
			case "unblock":
				//$fn_args['data']
				//mac
			
				$fn_url = "{$this->cl_cfg['unify']['unifiServer']}/api/s/{$this->cl_cfg['unify']['unifiSiteName']}/cmd/{$fn_type}-sta";
				
				$fn_data = json_encode($fn_args['data']);
				$fn_data_post = "json={$fn_data}";
			break;
			
			case "listsites":
				$fn_url = "{$this->cl_cfg['unify']['unifiServer']}/api/self/sites";
			break;
		}
		
		curl_setopt($ch, CURLOPT_URL, $fn_url);
		
		if($fn_data_post)
		{
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fn_data_post);
		}
		
		if(isset($this->fn_unify_cookie))
		{
			curl_setopt($ch, CURLOPT_COOKIESESSION, true);
	        curl_setopt($ch, CURLOPT_COOKIE, $this->fn_unify_cookie);
	    }
	    
	    $content = curl_exec($ch);
	    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers     = substr($content, 0, $header_size);
        $body        = trim(substr($content, $header_size));
        $http_code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        preg_match_all('|Set-Cookie: (.*);|Ui', $headers, $results);
        
        if(isset($results[1]))
        {
            $this->fn_unify_cookie = implode(';', $results[1]);
            if (!empty($body)) {
				if($this->cl_cfg['debug'])
				{
					print '<pre>';
					print PHP_EOL.'CODE COOKIE -----------------------------'.PHP_EOL;
					print PHP_EOL.$http_code.PHP_EOL;
					print PHP_EOL.'-----------------------------'.PHP_EOL;
					print '</pre>';
				}
            }
        }
		
		if($this->cl_cfg['debug'])
		{
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			if(($content = curl_exec($ch)) === false) error_log('curl error: ' . curl_error($ch));
		
			print '<pre>';
			print PHP_EOL.'-----LOGIN-------------------'.PHP_EOL;
			print_r (curl_getinfo($ch));
			print PHP_EOL.'-----RESPONSE----------------'.PHP_EOL;
			print $content;
			print PHP_EOL.'-----------------------------'.PHP_EOL;
			print '</pre>';
		}
		
		if($fn_type == 'unify')
		{
			self::callCurl('unifycmd', array(
				'data' => array(
					'up' => 1000,
					'down' => 1000,
					'cmd' => 'authorize-guest',
					'MBytes' => 400,
					'mac' => $this->cl_user_mac_id,
					'ap_mac' => $this->fn_ap_mac,
					'minutes' => 30,//$fn_args['time'],
				),
			));
		
			self::callCurl('unifylogout');
			sleep(6);
		}
		
		curl_exec($ch);
		curl_close ($ch);
	}
	
	/**
	 * removeOthersDaysDB function.
	 * Quitamos los mails pasados.
	 *
	 * @access private
	 * @return void
	 */
	private function removeOthersDaysDB()
	{
		$fn_time = date('Y-m-d', strtotime("{$this->fn_today} - 1 day"));
		
		$this->cl_db->Fetch("
			DELETE FROM `mails`
			WHERE `time`=:t;
		", array(
			't' => $fn_time
		));
	}
	
	/**
	 * generatePass function.
	 * Generate random password 
	 *
	 * @access private
	 * @return void
	 */
	private function generatePass()
	{
		srand(self::make_seed());
		
		$alfa = "23456789qwertyuiopasdfghjkzxcvbnmQWERTYUIPASDFGHJKLZXCVBNM";
		
		$password = "";
		
		for($i = 0; $i < 7; $i ++) 
		{
			$password .= $alfa[rand(0, strlen($alfa)-1)];
		}
		
		return $password;
	}
	
	/**
	 * make_seed function.
	 * Generate a random seed for use in password generation
	 *
	 * @access private
	 * @return void
	 */
	private function make_seed() 
	{
		list($usec, $sec) = explode(' ', microtime());
		return (float) $sec + ((float) $usec * 100000);
	}
	
	/**
	 * checkBan function.
	 * chekeamos el si hay ban 
	 *
	 * @access private
	 * @param mixed $fn_user
	 * @return true = ban / false = not ban
	 */
	private function checkBan($fn_user)
	{
		$fn_result_acp = false; //is banned by acp
		
		//create apn variables
		$this->fn_ip_loging = "{$_SERVER['SERVER_NAME']}~attemptbyip:{$this->cl_user_mac_id}";
		$this->fn_user_loging = "{$_SERVER['SERVER_NAME']}~atemptsbyuser:{$fn_user}";
		$this->fn_banattemp_loging = "{$_SERVER['SERVER_NAME']}~login-blocked:".md5("{$this->cl_user_mac_id}{$fn_user}");
		$this->fn_bantime_loging = "{$_SERVER['SERVER_NAME']}~login-blocked-time:".md5("{$this->cl_user_mac_id}{$fn_user}");
		
		//in debug mode apc remove instantaly
		if($this->cl_cfg['debug'])
		{
			//debug
			apc_delete($this->fn_ip_loging);
			apc_delete($this->fn_user_loging);
			apc_delete($this->fn_bantime_loging);
			apc_delete($this->fn_banattemp_loging);
		}
	
		//check by APC Cache -----------------
		$fn_apn_i = (!apc_exists($this->fn_ip_loging)) ? 0 : (int)apc_fetch($this->fn_ip_loging);
		$fn_apn_u = (!apc_exists($this->fn_user_loging)) ? 0 : (int)apc_fetch($this->fn_user_loging);
		
		$fn_apn_a = (!apc_exists($this->fn_banattemp_loging)) ? 0 : (int)apc_fetch($this->fn_banattemp_loging);
		$fn_apn_b = (!apc_exists($this->fn_bantime_loging)) ? false : apc_fetch($this->fn_bantime_loging); //timestamp ban time
		
		//print_r("ip:{$fn_apn_i} - user:{$fn_apn_u} - ban timestamp:{$fn_apn_b} - ");
		
		//unban check
		if($fn_apn_b && $fn_apn_a !== 0)
		{
			//check time apn
			if(strtotime($this->fn_now_date) > $fn_apn_b)
			{
				//unban
				self::unBan();
				
				return false;
			}
		}else{
			$fn_result_acp = true;
		}
		
		//many tries
		if($fn_apn_i >= 4 || $fn_apn_u >= 4) self::banTime($this->fn_banattemp_loging, $fn_user);
			
		//add items to cache
		apc_store($this->fn_ip_loging, $fn_apn_i+1, 86400);
		apc_store($this->fn_user_loging, $fn_apn_u+1, 86400);
		
		return $fn_result_acp;
	}
	
	/**
	 * banTime function.
	 * asignamos tiempo de baneo sengun los intentos
	 * 
	 * @access private
	 * @param mixed $fn_hash
	 * @param mixed $fn_username
	 * @return void
	 */
	private function banTime($fn_hash, $fn_username)
	{
		$fn_apn_b = (int)apc_fetch($fn_hash);
		
		if($fn_apn_b == false) $fn_apn_b = 1;
		apc_store($fn_hash, $fn_apn_b); //add attemps
		
		$fn_ban_time = self::attemsToTime($fn_apn_b);
		
		//ban by apc -----------------
		apc_store($this->fn_bantime_loging, strtotime($fn_ban_time));
		
		//un ban by unify
		self::callCurl('block', array(
			'mac' => $this->cl_user_mac_id,
		));
	}
	
	/**
	 * unBan function.
	 * 
	 * @access private
	 * @return void
	 */
	private function unBan()
	{
		//remove apc -----------------
		apc_delete($this->fn_ip_loging);
		apc_delete($this->fn_user_loging);
		apc_delete($this->fn_bantime_loging);
		apc_delete($this->fn_banattemp_loging);
		
		//un ban by unify
		self::callCurl('unblock', array(
			'mac' => $this->cl_user_mac_id,
		));
	}
	
	/**
	 * attemsToTime function.
	 * devuelve tiempo de baneo segun los intentos formato fecha
	 * 2^(x+1) mins: 2, 4, 8...
	 *
	 * @access private
	 * @param mixed $fn_attems
	 * @return void
	 */
	private function attemsToTime($fn_attems)
	{
		$fn_ban_time = pow(2, $fn_attems)*60;
		$fn_ban_time = date('Y-m-d H:i:s', strtotime("{$this->fn_now_date} + {$fn_ban_time} seconds"));
		
		return $fn_ban_time;
	}
}
	
?>